Preface

	Why does privacy and information security matter?

		https://old.reddit.com/r/privacy/wiki/index

	Why use free software?

		https://old.reddit.com/r/privacy/wiki/index#wiki_what_is_the_difference_between_libre.2C_foss.2C_closed_source.2C_gnu.2C_etc.3F__why_does_it_matter.3F__can_i_help_restore_privacy_even_if_i_use_windows_and_other_closed_source.3F

	How to avoid entrapment - if someone mentions breaking the law, disconnect and/or walk away

		if you mention anything illegal, I will refuse to do business with you.
	
Guides

	amnesiac systems

		how to install tails linux
	
			how to make a tails linux install medium
		
				optical discs are better, DVDs will fit the install image

					check the signature of the tails iso

	reasonably secure persistent systems

		qubes os

	backup system with xubuntu 18.04, zfs, and encrypted disk images

		use 6tb drives, and 2 or 3 extra drives for fault tolerance.

		how to backup data on your home network

	how to use meld to review source code differences

	how to generate passphrases and usernames securely
	
		how to combine /dev/random with die rolls and keyboard mashing
		
			sha256sum
			
		how to generate random usernames
		
		how to generate passphrases and usernames with only die rolls
		
			8-sided dice, and lists numbered in octal radix
			
		fault-tolerant key storage with xor and shamir secret sharing.
	
	how to use a password manager and keepassx
	
		always clear or replace your clipboard after pasting a password

	how to use GPG
	
		how to generate a keypair - don't use your real name
		
			how to generate a master key on an airgapped machine, and subkeys to use on non-airgapped machines.
		
		how to sign
		
		how to encrypt
		
			symmetric, asymmetric
			
	how to use electrum for digital signatures
	
		a bitcoin digital signature can fit in a short message or a microblogging service message.
			
	how to use firefox, ricochet, tor browser, and OnionShare
	
		use 2 or more instances of ricochet by removing the built-in binary and installing tor system-wide
		
			if you don't want some of your contacts to know when you're online, use a different instance of ricochet with them

		use high security on tor browser and block non-encrypted content

		how to browse the internet more safely

			use separate browser profiles on firefox

			remove google from firefox

			don't use a firefox account

			disable telemetry

			only use firefox for stuff that's broken on tor browser
		
	how to use airgapped devices, one-time-pads, cold signing, and cold encryption
	
		a true airgapped device should have no wireless interfaces, and should be running from a battery inside a faraday cage
		
			wireless interfaces can be removed from laptops
			
			laptops can be built without wireless interfaces
		
			wallpapering a room, its door, and its ceiling with tinfoil, and installing a metal floor, is a good way to make a faraday cage. the tinfoil should be grounded by connecting it to a metal water pipe or other ground wire. make sure to ventilate the room.
	
	how to get information into and out of airgapped devices with optical discs, qr codes, audio recordings, and mnemonic passphrases

		https://www.wired.com/2013/10/149481/

		optical discs are good for large amounts of data, but are vulnerable to firmware attacks
		
		qr codes and audio recordings may or may not be vulnerable to firmware attacks, but are cheaper for small amounts of data
		
		the safest way to get data in and out of airgapped machines is with mnemonic passphrases, where the user is aware of every bit being transferred.
		
	how to prepare to receive bitcoin
	
		how to generate a secure mnemonic on an airgapped machine
		
			how to install tails to a dvd or cd
		
			decoy wallets and deniability
			
				electrum seed extensions, sha256sum with additional words.
			
				practice lying, maybe play poker if you are not a gambling addict.
		
			how to store your mnemonic safely
			
				you can store 12 words in a bank vault, 12 in your head, and the xor of those 2 phrases in a secret place in your house.
		
		how to derive addresses from the mnemonic
		
			how to transfer the addresses out of the airgapped machine
			
				qr codes, and/or base58 or bech32 conversion library, and mnemonic phrases.
			
				how to generate a qr code from an address
		
		how to install a bitcoin full node on a laptop
		
			how to verify the binaries with gpg
		
		how to import watch-only addresses to the bitcoin full node from the airgapped machine 

		verify that you can derive the addresses from the mnemonic
		
		verify it again a week later
		
		verify it again
		
	how to receive bitcoin
	
		buy bitcoin
		
			atm without kyc
			
				have a qr code ready on paper
			
			in person with cash
			
				meet in a public place, have your qr code and address(es) ready
				
					check on your laptop's full node, or a block explorer, and wait for 1 confirmation.
					
						a block explorer is less secure, don't use the other person's device to verify.
			
			bisq
	
		earn bitcoin remotely
		
	how to not send bitcoin
	
		bitcoin is the best money humans have invented
		
			it is difficult to create more of it, easy to authenticate, easily divisible, durable, does not require permission to move.
			
				it is easy for a government to create more government money. to move digital government money requires permission. paper government money is not durable.
				
				gold is not easily divisible, and it is difficult to verify its authenticity.
			
			people tend to spend bad money and hold on to good money.
		
	how to send bitcoin
	
		use a gui privacy-focused wallet to automate coinjoins
		
		use joinmarket to pay from a coinjoin
		
			use joinmarket to pay from a coinjoin to open a lightning channel
			
		(less secure)
		
			how to open a lightning channel without a coinjoin
